import os
import glob
import mlpy

dir = "segments/AhuzaOct20-20141020_093624"

refs_array = []
for sfile in glob.glob(os.path.join(dir, "*-1.csv")):
	sf = open(sfile, 'r')
	prev_distance = -1
	sum_power = 0.0
	n = 0
	ref_array = []
	for line in sf:
		values = line.split("\t")
		try:
			power = float(values[9]) 
			distance = float(values[10]) 
		except ValueError:
			continue
		if distance == prev_distance:
			sum_power += power
			n += 1
		else:
			if n != 0:
				ref_array.append(sum_power/n)
			sum_power = power
			n = 1
			prev_distance = distance
	refs_array.append((sfile, ref_array))


for alpha in [0.05, 0.1, 0.3, 0.5]:
	print "*************" + str(alpha) +"*************"
	segs_array =  []
	for sfile in glob.glob(os.path.join(dir, "*-2.csv")):
		sf = open(sfile, 'r')
		seg_array = []
		avg_power = None
		for line in sf:
			values = line.split("\t")
			try:
				power = float(values[9]) 
			except ValueError:
				continue
			if avg_power == None:
				avg_power = power
			else:
				avg_power = (1-alpha)*avg_power + alpha*power
			seg_array.append(avg_power)
		segs_array.append((sfile,seg_array))

	total_prob_of_same = [0,0]
	total_prob_of_diff = [0,0]

	for i in segs_array:
		print i[0]
		prob_of_same = [0,0]
		prob_of_diff = [0,0]
		for j in refs_array:
			if i is not j:
				dist = mlpy.dtw_std(i[1], j[1])
				print "\t" + j[0] + ":" + str(dist)
				common_prefix = i[0][:[x[0]==x[1] for x in zip(i[0],j[0])].index(0)]
				if len(common_prefix) > 0 and common_prefix[-1] == '-': #same segment
					print "\tSame!"
					prob_of_same[0] += 1
					prob_of_same[1] += 1/dist
				else:
					prob_of_diff[0] += 1
					prob_of_diff[1] += 1/dist
		
		prob_same = (prob_of_same[1]/(prob_of_same[1] + prob_of_diff[1])/prob_of_same[0])
		prob_diff = (prob_of_diff[1]/(prob_of_same[1] + prob_of_diff[1])/prob_of_diff[0])
		print "Probability of same :" + str(prob_same) + "\n"
		print "Probability of diff :" + str(prob_diff) + "\n"
		total_prob_of_same[0] += 1 
		total_prob_of_diff[0] += 1
		total_prob_of_same[1] += prob_same
		total_prob_of_diff[1] += prob_diff
		
	print "Average probability with same segments: " + str(total_prob_of_same[1]/total_prob_of_same[0]) + "\n"
	print "Average probability with different segments: " + str(total_prob_of_diff[1]/total_prob_of_diff[0]) + "\n"
