function test_tracking
    % Tracking using particle filters
    RECORD_MOVIE = false;
    
    %% Read logs and test file
    LOG_DIR = '../../BatLevelLogs';
    TEST_DEVICE = '0094e779d7d1841f';
    TEST_FILENAME = 'tosb-20140619_190335.csv';
%     TEST_DEVICE = '04dc22d4dad7e4ce';
%     TEST_FILENAME = 'tosb-20140626_145409.csv';
    
    PLOT_DATA = true;
    RECORD_MOVIE = false;
    FPS = 10;
    
    assert(PLOT_DATA || ~RECORD_MOVIE);
    
    log_list = get_log_list(TEST_DEVICE, TEST_FILENAME);
    
    %% Convert log list to profiles
    profiles = cell(length(log_list));
    power_data = cell(length(log_list));
    for i = 1:length(profiles)
       profiles{i} = create_profile(log_list.Data{i});
       power_data{i} = profiles{i}(:, 4);
    end
    
    aligned_profiles = align_data(power_data, false);
    
    %% Read test data
    test_file = [LOG_DIR '/' TEST_DEVICE '/' TEST_FILENAME];
    test_data = read_data(test_file);
    test_profile = create_profile(test_data);
    
    power_plot = figure;
    
    %% Particle filter code starts here
    NUM_PARTICLES = 1000;
    particle_filter = ParticleFilter(power_data, NUM_PARTICLES);

    STEP_SIZE = 10;
    PARTICLE_PCNTL = 99;
    
    ind_range = 50:floor(length(test_profile)/STEP_SIZE);
    time = zeros(size(ind_range));
    
    if RECORD_MOVIE
        movie_frames(length(ind_range)) = struct('cdata',[],'colormap',[]);
    end

    true_lon = zeros(size(ind_range));
    true_lat = zeros(size(ind_range));
    est_lon = zeros(size(ind_range));
    est_lat = zeros(size(ind_range));
    
    progressbar;
    h = figure;
    for i = ind_range
        position = 1+(i-1)*STEP_SIZE;
        time(i) = test_profile(position, 1);

        true_lon(i) = test_profile(position, 2);
        true_lat(i) = test_profile(position, 3);
        
        observation = test_profile(position, 4);
        particle_filter.update(observation, STEP_SIZE);

        if PLOT_DATA
            figure(h);
            scatter(test_profile(:, 2), test_profile(:, 3), 'fill');
            hold all;
            
            p_lon = zeros(NUM_PARTICLES, 1);
            p_lat = zeros(NUM_PARTICLES, 1);
            
            for prof = 1:length(profiles)
                ind = particle_filter.routes == prof;
                p_lon(ind) = profiles{prof}(particle_filter.offset(ind), 2);
                p_lat(ind) = profiles{prof}(particle_filter.offset(ind), 3);
            end
            
%             [~, chosen] = max(particle_filter.weights);
            chosen = find(particle_filter.weights >= ...
                prctile(particle_filter.weights, PARTICLE_PCNTL));
            
            scatter(p_lon(chosen), p_lat(chosen), 200, 'g', 'fill')
                
            scatter(true_lon(i), true_lat(i), 100, 'r', 'fill');
            hold off;
            
            figure(power_plot);
            plot(test_profile(:, 4));
            hold all;
            plot(aligned_profiles{1});
            scatter(position, test_profile(position, 4), 100, 'r', 'fill');
            hold off;
        end
        
        if RECORD_MOVIE
            movie_frames(i) = getframe(h);
        end
        
        progressbar(i/length(ind_range));
    end
    
    if RECORD_MOVIE
        movie2avi(movie_frames, 'tracking.avi', 'FPS', FPS);
    end
end

function profile = create_profile(data)
    preprocessed_power = preprocess_data(data.Power, ...
        'SmoothingWindow', 200);
    profile = [data.RelativeTime data.Longitude data.Latitude preprocessed_power];
    profile = downsample(profile, 5);
end

function log_list = get_log_list(test_device, test_filename)
    [log_list, ~] = read_log_list('tosb_logs.csv');
    
    log_filter = strcmp(log_list.GPS, 'Yes') ...
        & strcmp(log_list.Current, 'Yes') ...
        & ~strcmp(log_list.Charging, 'Yes') ...
        & strcmp(log_list.Route, 'tosb') ...
        & ~strcmp(log_list.Device, test_device) ...
        & ~strcmp(log_list.Filename, test_filename);

    filtered_log_list = log_list{log_filter, :};
    log_list = cell2table(filtered_log_list, 'VariableNames', ...
        log_list.Properties.VariableNames);

    fprintf('%d entries in filtered log list\n', height(log_list));
end