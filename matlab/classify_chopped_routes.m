function mcr = classify_chopped_routes(loglist_filename)
    USE_CACHE = true;
    
    %% Classification evaluation
    if ~USE_CACHE
        log_list = read_log_list(loglist_filename);
        log_list_filter = ~strcmp(log_list.DeviceType, 'GalaxyS3');
        log_list = log_list(log_list_filter, :);
        
        [distance_matrix, ~] = preprocess_and_compute_distance(log_list);
        save distance_matrix.mat
    else
        load distance_matrix.mat
    end
    
    fprintf('%d entries in %s\n', length(log_list), loglist_filename);
    fprintf('%d unique routes\n', length(unique(log_list.Label)));
    
    NUM_PER_CLASS = 3; % minimal number of train samples per class

    % Chose random train and test sets
    mcr = evaluate_distance_matrix(log_list, distance_matrix, NUM_PER_CLASS);
end