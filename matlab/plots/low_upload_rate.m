function low_upload_rate
   addpath('..');
    LOG_DIR = '../../../BatLevelLogs';
    NEXUS_4_1 = '00a697fa469633a5';
    NEXUS_4_2 = '0094e779d7d1841f';
    NEXUS_4_3 = '04dc22d4dad7e4ce';
    NEXUS_5 = '042335963089f20b';
    
    SMOOTHING_WINDOW = 200;
    DOWNSAMPLE_FACTOR = 50;
    
    nexus_data_1 = read_data([LOG_DIR '/' NEXUS_4_1 '/todasha-20141108_154652.csv']);
    nexus_data_2 = read_data([LOG_DIR '/' NEXUS_4_2 '/todasha-20141108_154738.csv']);
    nexus_data_3 = read_data([LOG_DIR '/' NEXUS_4_3 '/todasha-20141108_154616.csv']);
    nexus_data_4 = read_data([LOG_DIR '/' NEXUS_5 '/todasha-20141108_154712.csv']);
    
    processed_power_1 = preprocess_data(nexus_data_1.Power, ...
        'SmoothingWindow', SMOOTHING_WINDOW, ...
        'DownsampleFactor', DOWNSAMPLE_FACTOR);
    processed_power_2 = preprocess_data(nexus_data_2.Power, ...
        'SmoothingWindow', SMOOTHING_WINDOW, ...
        'DownsampleFactor', DOWNSAMPLE_FACTOR);
    
    aligned_data = align_data({processed_power_1, processed_power_2}, false);
    
    plot(aligned_data{1}(10:end-10), 'r-');
    hold all;
    plot(aligned_data{2}(10:end-10), 'bx');
    xlabel('Samples');
    ylabel('Normalized Power');
    axis tight;
    legend('Device 1', 'Device 2');
    set(gca, 'box', 'off'); 
end