function compare_same_model_profiles
    addpath('..');
    LOG_DIR = '../../../BatLevelLogs';
    NEXUS_4_1 = '00a697fa469633a5';
    NEXUS_4_2 = '0094e779d7d1841f';
    NEXUS_4_3 = '04dc22d4dad7e4ce';
    
    nexus_data_1 = read_data([LOG_DIR '/' NEXUS_4_1 '/tosb-20140623_131057.csv']);
    nexus_data_2 = read_data([LOG_DIR '/' NEXUS_4_2 '/tosb-20140623_131114.csv']);
    
    range1 = 1500:(length(nexus_data_1.RelativeTime)-150);
    range2 = 1500:(length(nexus_data_2.RelativeTime)-150);
    
    smoothed_power1 = smooth(nexus_data_1.Power, 200);
    plot(nexus_data_1.RelativeTime(range1)/1000, ...
        smoothed_power1(range1));
    hold all;
    smoothed_power2 = smooth(nexus_data_2.Power, 200);
    plot(nexus_data_2.RelativeTime(range2)/1000, ...
        smoothed_power2(range2), 'r');
    axis tight;
    xlabel('Time [sec]', 'FontSize', 15);
    ylabel('Power [Watt]', 'FontSize', 15);
    legend('Device 1', 'Device 2');
    set(gca, 'box', 'off');
end