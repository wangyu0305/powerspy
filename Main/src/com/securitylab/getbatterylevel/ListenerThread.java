package com.securitylab.getbatterylevel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class ListenerThread extends Thread {
	
	private static final int LOCATION_MIN_DISTANCE = 10;
	private static final int LOCATION_MIN_TIME = 500;
	private static final int THREAD_SLEEP_TIME = 10;
	private static final String LOG_EXT = ".csv";
	
	private Service m_service;
	
	private TelephonyManager m_telephonyMgr;
	private LocationManager mlocationMgr;
	
	private int signalstrength = -1; 
	private int batlevel = -1;
	private int batvolt = -1;
	private int battemp = -1;
	private int batamp = -1;
	private double latitude = -1;
	private double longitude = -1;
	private int cellId = -1;
	
	private boolean m_saveLogToFile = false;
	private String m_outputFilename;
	private PrintWriter m_outputWriter;
	
    private boolean cbGPS = false;
    private boolean cbBattery = false;
    private boolean cbSignalStrength = false;
    private String Comment;
    private PowerManager.WakeLock lock;
    private PowerManager pm;
    private Context ctx;
    
    private boolean m_execute = true;
    
	public ListenerThread(Service service, boolean cbGPS, boolean cbBattery, boolean cbSignalStrength, boolean saveLog, String Comment) {
		super("ListenerThread");
		
		m_service = service;
		
		this.cbGPS = cbGPS;
		this.cbBattery = cbBattery;
		this.cbSignalStrength = cbSignalStrength;
		this.Comment = Comment;
		this.m_saveLogToFile = saveLog;
		ctx = App.getAppContext();
		
		//keep the phone partially awake (screen goes black but CPU still running).
	    pm = (PowerManager)ctx.getApplicationContext().getSystemService(Context.POWER_SERVICE);
	    lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ListenerThread lock");
	    lock.setReferenceCounted(true);
	}
	
	@SuppressLint("Wakelock")
	public void run()
	{
		Log.d(Constants.TAG, "Starting Thread....");

		// acquire wake lock to prevent CPU from sleeping
		lock.acquire();
		
		if (m_saveLogToFile) {
			InitializeOutputFile();
		}
		
		StartListening();
		
		//Finished initializations. Now wait until interrupted.
		
		while (m_execute) {
			if (cbBattery)
			{
				ProbeBattery();
			}
		    try {
		        Thread.sleep(THREAD_SLEEP_TIME);
		    } catch (InterruptedException e) {
		    	m_execute = false;
		    }
		}
		
        StopListening();
        lock.release();
        Log.d(Constants.TAG, "Stoping Thread....");
	} // end of run
	
	public void quit()
	{
		m_execute = false;
	}

	@SuppressLint("SimpleDateFormat")
	private void InitializeOutputFile()
	{
		//Create output directory
		final String outputDirname = Environment.getExternalStorageDirectory().toString()+"/BatLevel";
		File outputDir = new File(outputDirname);
		if (!outputDir.exists()) {
			Log.d(Constants.TAG, "Creating output directory");
			outputDir.mkdir();
		}
		
		createOutputFile(outputDirname);
	}

	private void createOutputFile(String outputDir) {
		//Create output file
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
		final String currentDateandTime = sdf.format(new Date());
		
		String tmpText =  Comment;
		if (!tmpText.equals("")) {
			tmpText += "-";
		}
		tmpText += currentDateandTime;
		
		File outputFile;
		int i = 0;
		do {
			m_outputFilename = outputDir + "/" +  tmpText + LOG_EXT;
			outputFile = new File(m_outputFilename);
			++i;
			tmpText = Comment + "-" + i;
		} while (outputFile.exists());

		Log.d(Constants.TAG, m_outputFilename);
	    
		writeHeader();
	}
	
	//Write header to output file
	private void writeHeader() {
		try {
			m_outputWriter = new PrintWriter(new BufferedWriter(new FileWriter(m_outputFilename, false)));
		} catch (IOException e) {
			Log.e(Constants.TAG, e.getMessage());
			e.printStackTrace();
		}
		
		// Signal - Signal strength
		// SOD - State of Discharge
		String str = String.format("Time\tSOD\tVolt\tCurrent\tTemp\tSignal\tLatitude\tLongitude\tCellID");
		m_outputWriter.println(str);
		m_outputWriter.flush();
	}

	public void StartListening()
	{
		m_telephonyMgr = (TelephonyManager) m_service.getSystemService(Context.TELEPHONY_SERVICE);
		mlocationMgr = (LocationManager) m_service.getSystemService(BackgroundRecorder.LOCATION_SERVICE);
		
		if (cbGPS) {
			try {
				mlocationMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_MIN_TIME, LOCATION_MIN_DISTANCE, 
						onLocationChange, Looper.getMainLooper());
			} catch (Exception e) {
				Log.e(Constants.TAG, e.getMessage());
				e.printStackTrace();
			}
		}
				
		if (cbSignalStrength) {
			m_telephonyMgr.listen(myPhoneStateListener,
					PhoneStateListener.LISTEN_SIGNAL_STRENGTHS +
					// we're interested in current cell the device is connected to
					// therefore not listening to cell info
					// PhoneStateListener.LISTEN_CELL_INFO + 
					PhoneStateListener.LISTEN_CELL_LOCATION);
		}
		
	}
	
	
	private void updateLog()
	{
		Log.v(Constants.TAG, "Updating log...");
		long currTime = System.currentTimeMillis();
		
		//send record string to UI
    	String record = "Time " + String.valueOf(currTime) + "\nLevel " + String.valueOf(batlevel) + 
    			"\nVolt " + String.valueOf(batvolt) + "\nAmp " + String.valueOf(batamp) + "\nTemp " + String.valueOf(battemp) 
    			+ "\nSignal strength " + String.valueOf(signalstrength) 
    			+ "\nLatitude " + String.valueOf(latitude) + "\nLongtitude " + String.valueOf(longitude) + 
    			"\nCell ID " + String.valueOf(cellId) + ".";
    	
		Message msg = Message.obtain();
		msg.obj = record;
		Main.m_inhandler.sendMessage(msg);
		
		if (!m_saveLogToFile) {
			return;
		}
		
	    final String logLine = prepareLogLine(currTime);
	    m_outputWriter.println(logLine);
	    // m_outputWriter.flush();
	} // end of updateLog

	private String prepareLogLine(long currTime) {
		String str = currTime + "\t" + batlevel + "\t" + batvolt + "\t" + batamp + "\t" + battemp + "\t" + signalstrength
				+ "\t" + latitude + "\t" + longitude + "\t" + cellId;
		return str;
	}
	
	public PhoneStateListener myPhoneStateListener = new PhoneStateListener() {
		public void onSignalStrengthsChanged(SignalStrength signalStrength)
		{
			signalstrength = signalStrength.getGsmSignalStrength();
			updateLog();
		}
		
		public void onCellLocationChanged(CellLocation location)
		{
			GsmCellLocation gsmCellLocation = (GsmCellLocation)location;
			cellId = gsmCellLocation.getCid(); //registered cell is starred
			updateLog();
		}
	};
	
	private void StopListening() {
		Log.d(Constants.TAG, "Handling stop");
		
		if (cbSignalStrength) {
			m_telephonyMgr.listen(myPhoneStateListener, PhoneStateListener.LISTEN_NONE);
		}
		
		if (cbGPS) {
			mlocationMgr.removeUpdates(onLocationChange);
		}
		
		signalstrength = -1; 
		batlevel = -1;
		batvolt = -1;
		battemp = -1;
		batamp = -1;
		latitude = -1;
		longitude = -1;
		cellId = -1;
		
		if (null != m_outputWriter) {
			m_outputWriter.close();
			m_outputWriter = null;
		}
	} // end of StopListening

    public LocationListener onLocationChange = new LocationListener() {
        public void onLocationChanged(Location loc) {
          latitude = loc.getLatitude();
          longitude = loc.getLongitude();
          updateLog();
        }

		@Override
		public void onProviderDisabled(String arg0) {
		}
		
		@Override
		public void onProviderEnabled(String arg0) {
		}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		}
    };

    private void ProbeBattery()
    {
    	Log.v(Constants.TAG, "Probing battery...");
        try {
           	int new_amp = getNumberFromFile(Constants.SYS_CLASS_CURRENT_NOW);
           	int new_vol = getNumberFromFile(Constants.SYS_CLASS_VOLTAGE_NOW);
           	int new_lev = getNumberFromFile(Constants.SYS_CLASS_CAPACITY);
           	int new_tmp = getNumberFromFile(Constants.SYS_CLASS_TEMP);
           	boolean shouldUpdateLog = false;
            if (new_amp != batamp)
            {
            	batamp = new_amp;
            	shouldUpdateLog = true;
            }
            if (new_vol != batvolt)
            {
            	batvolt = new_vol;
            	shouldUpdateLog = true;
            }
            if (new_tmp != battemp)
            {
            	battemp = new_tmp;
            	shouldUpdateLog = true;
            }
            if (new_lev != batlevel)
            {
            	batlevel = new_lev;
            	shouldUpdateLog = true;
            }

            if (shouldUpdateLog) {
            	updateLog();
            }
        }
        catch (IOException e) {
        	Log.e(Constants.TAG, e.getMessage());
            e.printStackTrace();
        }
    } // end of ProbeBattery

	private String convertStreamToString(InputStream is) throws IOException {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line);//.append("\n");
	    }
	    reader.close();
	    return sb.toString();
	}

	private int getNumberFromFile (String filePath) throws IOException {
	    File fl = new File(filePath);
	    FileInputStream fin = new FileInputStream(fl);
	    String ret = convertStreamToString(fin);
	    // Make sure we close all streams.
	    fin.close();        
	    int value = Integer.parseInt(ret);
	    return value;
	}

} // end of ListenerThread
