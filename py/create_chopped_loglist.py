#!/usr/bin/env python

# Create log list of chopped routes

import sys
import os
import pandas
import re

DEVICES = {
'0076296b4ed758ef' : 'Nexus5',
'0094e779d7d1841f' : 'Nexus4',
'00a697fa469633a5' : 'Nexus4',
'042335963089f20b' : 'Nexus5',
'04dc22d4dad7e4ce' : 'Nexus4',
'087a1d930591f4ca' : 'Nexus5',
'3230df8e10eb10ed' : 'GalaxyS3'}

def create_loglist(log_dir):
    print 'Generating log list for', log_dir

    columns = ['Device', 'DeviceType', 'Route', 'Filename']
    df = pandas.DataFrame(columns=columns)

    for device in DEVICES.keys():
        directory = os.path.join(log_dir, device)
        if not os.path.exists(directory):
            continue

        route_logs = os.listdir(directory)
        for route_file in route_logs:
            # print route_file
            route_part1 = route_file.split('-')[0]
            route_part2 = '_'.join(route_file.split('.')[0].split('_')[-2:])
            route = route_part1 + '_' + route_part2
            row = {'Device' : device, 'DeviceType' : DEVICES[device], 'Route' : route, 'Filename' : route_file}
            df = df.append(row, ignore_index=True)

    return df

def main():
    log_dir = sys.argv[1]
    output_file = sys.argv[2]
    log_list = create_loglist(log_dir)
    log_list.to_csv(output_file, sep='\t', index=False)

if __name__ == '__main__':
    main()
